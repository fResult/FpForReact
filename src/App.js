import React, {Component} from 'react';
import './App.css';
import AddUserForm from "./components/add-person-form";
import PersonList from "./components/person-list";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: [{id: 1576788612950, firstName: 'Anakin', lastName: 'Skywalker', sex: 'Female'}],
    };
  }

  handleAddPerson = (person) => {
    const {persons} = this.state;
    persons.push({id: Date.now(), firstName: person.firstName, lastName: person.lastName, sex: person.sex});
  };

  handleRemovePerson = (personId) => {
    let {persons} = this.state;
    persons.forEach((person, idx) => {
      if (person.id === personId) persons = persons.splice(idx, 1)
    })
  };

  render() {
    return (
        <div style={{height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'space-evenly'}}>
          <AddUserForm onAddPerson={"pass handleAddPerson function to AddUserForm component"}/>
          <PersonList onRemovePerson={"pass handleRemovePerson function to PersonList component"}/>
        </div>
    );
  }
}

export default App;

