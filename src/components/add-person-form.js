import React, {Component} from 'react';

import styles from './add-person-form.module.css';

class AddPersonForm extends Component {

  state = {
    firstName: '',
    lastName: '',
    sex: 'Male'
  };


  render() {
    return (
        <>
          <form>
            <div>
              <label>First Name:&nbsp;&nbsp;</label>
              <br/>
              <input onChange={"Pass handleChangeFirstName to Event Listener onChange"}/>
            </div>
            <div>
              <label>Last Name:&nbsp;&nbsp;</label>
              <br/>
              <input onChange={"Pass handleChangeLastName to Event Listener onChange"}/>
            </div>
            <div>
              <label>Sex:&nbsp;&nbsp;</label>
              <br/>
              <select onChange={"Pass handleChangeSex to Event Listener onChange"} defaultValue="Male">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
            </div>
            <br/>
            <div style={{textAlign: 'center'}}>
              <button type="button" className={styles.BtnAdd} onClick={"Pass handleAddUser to Event Listener onClick"}>
                Add People
              </button>
            </div>
          </form>
        </>
    );
  }
}

export default AddPersonForm;
