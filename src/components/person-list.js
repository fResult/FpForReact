import React, {Component} from 'react';
import styles from './person-list.module.css';

class PersonList extends Component {
  render() {
    return (
        <>
          <table style={{border: 'dodgerblue 1px solid'}}>
            <thead>
            <tr>
              <th className={styles.TableHeader}>ID</th>
              <th className={styles.TableHeader}>First Name</th>
              <th className={styles.TableHeader}>Last Name</th>
              <th className={styles.TableHeader}>Sex</th>
              <th className={styles.TableHeader}>Actions</th>
            </tr>
            </thead>

            <tbody>
            <tr>
              <td className={`${styles.TableData} ${styles.CenterCell}`}>
                1576790131259
              </td>
              <td className={styles.TableData}>
                Anakin
              </td>
              <td className={styles.TableData}>
                Skywalker
              </td>
              <td className={`${styles.TableData} ${styles.CenterCell}`}>
                Female
              </td>
              <td className={`${styles.TableData} ${styles.CenterCell}`}>
                <button className={styles.BtnRemove}
                        onClick={"pass handleRemovePerson function to Event Listener"}>
                  Remove
                </button>
              </td>
            </tr>
            </tbody>
          </table>
        </>
    );
  }
}

export default PersonList;
